'use strict';

const {
    webpack,
    createConfig,
    devServer,
    defineConstants,
    env,
    entryPoint,
    setOutput,
    setDevTool,
    sourceMaps,
    addPlugins
} = require('webpack-blocks');

const ts = require('webpack-blocks-ts');
const vue = require('webpack-blocks-vue');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const autoprefixer = require('autoprefixer');

const basePlugins = [
    // Generate skeleton HTML file
    new HtmlWebpackPlugin({
        inject: true,
        template: 'index.html'
    })
];

module.exports = createConfig([
    entryPoint('./scripts/script.ts'),

    setOutput('./build/bundle-[hash].js'),

    ts({ appendTsSuffixTo: [/\.vue$/] }),

    vue({
        esModule: true,
        postcss: [autoprefixer()]
    }),

    defineConstants({
        'process.env.NODE_ENV': process.env.NODE_ENV
    }),

    addPlugins(basePlugins),

    env('development', [
        devServer(),
        sourceMaps()
    ]),
]);
