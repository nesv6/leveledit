export enum EntityKind {
    Checkpoint,
    Enemy,
}

export class Entity {
    kind:     EntityKind;
    position: { x: number, y: number };
    data:     number;

    constructor(kind: EntityKind, position: { x: number, y: number},
                data: number) {
        this.kind     = kind;
        this.position = position;
        this.data     = data;
    }
}
