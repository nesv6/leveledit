import App from '../components/app.vue';
import Map from './map';
import {EntityKind} from './entity';

export default class {
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;

    app: App;
    map: Map;

    mouseDown: boolean = false;

    clickCollision: boolean = false;
    clickDeath:     boolean = false;

    hoverPos: { x: number, y: number } = { x: 0, y: 0 };

    drawSpecial() {
        for (let y = 0; y < 30; y++) {
            for (let x = 0; x < 32; x++) {
                this.context.fillStyle = 'rgba(255, 255, 255, 0.4)'
                if (this.map.collision[y][x])
                    this.context.fillRect(x * 16, y * 16, 16, 16);

                this.context.fillStyle = 'rgba(255, 0, 0, 0.6)'
                if (this.map.death[y][x])
                    this.context.fillRect(x * 16, y * 16, 16, 16);
            }
        }
    }

    draw() {
        const image   = this.context.createImageData(512, 480);
        const hover_x = this.hoverPos.x;
        const hover_y = this.hoverPos.y;

        for (let y = 0; y < 30; y++) {
            for (let x = 0; x < 32; x++) {
                const val    = this.map.level[y][x];
                const sprite = this.app.sprites[val + 256];

                let attr = this.map.attributes[y >> 1][x >> 1];
                if (this.app.selectedColor >= 0 && x >> 1 == hover_x && y >> 1 == hover_y)
                    attr = this.app.selectedColor;

                this.app.drawSprite(image, x * 8, y * 8, sprite, attr, 2);
            }
        }

        if (this.app.selectedSprite >= 0 && hover_x >= 0 && hover_y >= 0) {
            const attr   = this.map.attributes[hover_y >> 1][hover_x >> 1];
            const sprite = this.app.sprites[this.app.selectedSprite + 256];
            this.app.drawSprite(image, hover_x * 8, hover_y * 8, sprite, attr, 2);
        }

        this.context.putImageData(image, 0, 0);

        for (let entity of this.map.entities) {
            const x = entity.position.x;
            const y = entity.position.y;

            switch (entity.kind) {
            case EntityKind.Checkpoint:
                this.context.strokeStyle = '#ff00ff';
                this.context.strokeRect(x * 2, y * 2, 32, 32);
            }
        }

        if (this.app.selectedSpecial)
            this.drawSpecial();
    }

    onMouseMoveColor(event: MouseEvent) {
        this.hoverPos = this.app.getMousePos(this.canvas, event, 32);

        if (this.mouseDown && this.app.selectedColor >= 0) {
            this.map.attributes[this.hoverPos.y][this.hoverPos.x] =
                this.app.selectedColor;
        }

        this.draw();

        this.context.strokeStyle = '#fff';
        this.context.strokeRect(this.hoverPos.x * 32, this.hoverPos.y * 32, 32, 32);

        this.context.beginPath();
        // Horizontal subdivision line
        this.context.moveTo(this.hoverPos.x * 32,      this.hoverPos.y * 32 + 16);
        this.context.lineTo(this.hoverPos.x * 32 + 32, this.hoverPos.y * 32 + 16);
        // Vertical subdivision line
        this.context.moveTo(this.hoverPos.x * 32 + 16, this.hoverPos.y * 32);
        this.context.lineTo(this.hoverPos.x * 32 + 16, this.hoverPos.y * 32 + 32);

        this.context.setLineDash([2, 2]);
        this.context.stroke();
        this.context.setLineDash([]);
    }

    onMouseMoveSprite(event: MouseEvent) {
        this.hoverPos = this.app.getMousePos(this.canvas, event, 16);

        if (this.mouseDown) {
            if (this.app.selectedSprite >= 0)
                this.map.level[this.hoverPos.y][this.hoverPos.x] = this.app.selectedSprite;
            else if (this.app.selectedSpecial == 'collision')
                this.map.collision[this.hoverPos.y][this.hoverPos.x] = !this.clickCollision
            else if (this.app.selectedSpecial == 'death')
                this.map.death[this.hoverPos.y][this.hoverPos.x] = !this.clickDeath
        }

        this.draw();

        this.context.strokeStyle = '#fff';
        this.context.strokeRect(this.hoverPos.x * 16, this.hoverPos.y * 16, 16, 16);
    }

    onMouseMove(event: MouseEvent) {
        if (this.app.selectedColor >= 0)
            this.onMouseMoveColor(event);
        else
            this.onMouseMoveSprite(event);
    }

    onClick(event: MouseEvent) {
        const pos = this.app.getMousePos(this.canvas, event, 16);
        if (this.app.selectedSprite >= 0)
            this.map.level[pos.y][pos.x] = this.app.selectedSprite;
        else if (this.app.selectedColor >= 0)
            this.map.attributes[pos.y >> 1][pos.x >> 1] = this.app.selectedColor;
        else if (this.app.selectedSpecial == 'collision')
            this.map.collision[pos.y][pos.x] = !this.clickCollision
        else if (this.app.selectedSpecial == 'death')
            this.map.death[pos.y][pos.x] = !this.clickDeath

        this.draw();
    }

    onContextMenu(event: MouseEvent) {
        event.preventDefault();
        if (this.app.selectedSprite < 0)
            return;

        const clickPos = this.app.getMousePos(this.canvas, event, 16);
        const origSprite = this.map.level[clickPos.y][clickPos.x];
        if (origSprite == this.app.selectedSprite)
            return;

        let stack: { x: number, y: number }[] = [clickPos];
        let pos: { x: number, y: number } | undefined;
        while (pos = stack.pop()) {
            if (this.map.level[pos.y][pos.x] != origSprite)
                continue;

            this.map.level[pos.y][pos.x] = this.app.selectedSprite;
            for (let y of [-1, 0, 1]) {
                const newY = y + pos.y;
                if (newY < 0 || newY >= 30)
                    continue;

                for (let x of [-1, 0, 1]) {
                    const newX = x + pos.x;
                    if (newX < 0 || newX > 32)
                        continue;

                    if (this.map.level[newY][newX] == origSprite)
                        stack.push({ x: newX, y: newY });
                }
            }
        }

        this.draw();
    }

    onMouseDown(event: MouseEvent) {
        this.mouseDown = true;

        const pos = this.app.getMousePos(this.canvas, event, 16);
        this.clickCollision = this.map.collision[pos.y][pos.x];
        this.clickDeath     = this.map.death[pos.y][pos.x];
    }

    constructor(app: App) {
        this.app = app;
        this.map = this.app.map;

        this.canvas = document.querySelector('.screen-canvas') as HTMLCanvasElement;

        const context = this.canvas.getContext('2d');
        if (!context)
            throw 'Could not get 2D context';
        this.context = context;

        this.canvas.addEventListener('mousedown',   (e: MouseEvent) => this.onMouseDown(e));
        this.canvas.addEventListener('mouseup',     (e: MouseEvent) => this.mouseDown = false);
        this.canvas.addEventListener('mouseleave',  (e: MouseEvent) => this.mouseDown = false);
        this.canvas.addEventListener('mousemove',   (e: MouseEvent) => this.onMouseMove(e));
        this.canvas.addEventListener('click',       (e: MouseEvent) => this.onClick(e));
        this.canvas.addEventListener('contextmenu', (e: MouseEvent) => this.onContextMenu(e));
    }
}
