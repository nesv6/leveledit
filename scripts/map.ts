import App from '../components/app.vue';
import {EntityKind, Entity} from './entity';

export default class Map {
    level:       number[][]  = [];
    attributes:  number[][]  = [];
    entities:    Entity[]    = [];
    palettes:    number[]    = [];
    collision:   boolean[][] = [];
    death:       boolean[][] = [];

    mapData: string;

    constructor() {
        this.mapData = localStorage.getItem('mapData') || '';
        this.deserialize();
    }

    serialize() {
        // Re-tangle the attributes
        let attr: number[] = [];
        for (let y = 0; y < 15; y += 2) {
            for (let x = 0; x < 16; x += 2) {
                let value = (this.attributes[y][x] << 0) | (this.attributes[y][x + 1] << 2);
                if (y < 14)
                    value |= (this.attributes[y + 1][x] << 4) | (this.attributes[y + 1][x + 1] << 6);

                attr.push(value);
            }
        }

        let flat: number[] = [];
        for (let row of this.level)
            flat = flat.concat(row);
        flat = flat.concat(attr);

        let current = flat[0];
        let count = 0;

        let result = this.palettes.concat(0xFF);

        // Compress the level data
        let compressed: number[] = []
        for (let tile of flat) {
            if (current == tile && count < 256)
                count++;
            else if (count > 0) {
                compressed.push(count % 256);
                compressed.push(current);

                count = 1;
                current = tile;
            }
        }

        if (count > 0) {
            compressed.push(count % 256);
            compressed.push(current);
        }

        // Join everything together
        while (compressed.length) {
            let slice = compressed.splice(0, 254);
            result = result.concat(slice.length, slice);
        }
        result.push(0);

        // Add the collision bitmap
        let flatBits: boolean[] = [];
        for (let row of this.collision)
            flatBits = flatBits.concat(row);

        for (let i = 0; i < flatBits.length; i += 8) {
            let byte = 0;
            for (let j = 0; j < 8; j++)
                byte |= flatBits[i + j] ? (0x80 >> j) : 0;
            result.push(byte);
        }

        // Add the death bitmap
        flatBits = [];
        for (let row of this.death)
            flatBits = flatBits.concat(row);

        for (let i = 0; i < flatBits.length; i += 8) {
            let byte = 0;
            for (let j = 0; j < 8; j++)
                byte |= flatBits[i + j] ? (0x80 >> j) : 0;
            result.push(byte);
        }

        // Add the entities
        result.push(this.entities.length);
        for (let entity of this.entities as Entity[]) {
            result.push(entity.kind)
            result.push(entity.position.x)
            result.push(entity.position.y)
            result.push(entity.data)
        }

        // Convert the array to C syntax
        this.mapData = new Uint8Array(result).join(',')
        localStorage.setItem('mapData', this.mapData);
    }

    deserialize() {
        if (!this.mapData)
            return;

        // Parse the array into an array of numbers
        const cleaned: string[] = this.mapData.replace(/{([\s\S]*)};?/gm, '$1').split('\n');
        const noComments: string[] = cleaned.map((a: string) => a.replace(/\/\/.*$/, '').replace(/ /g, ''));
        const numbers: number[] = noComments.join('').split(',').map((x: string) => parseInt(x));

        // Load the palettes
        let i: number;
        this.palettes = [];
        for (i = 0; numbers[i] != 0xFF; i++)
            this.palettes.push(numbers[i]);
        numbers.splice(0, i + 1);

        // Decompress the level and attribute data
        let flat: number[] = [];
        while (numbers) {
            const length = numbers.splice(0, 1)[0];
            if (length == 0)
                break;

            const block = numbers.splice(0, length);
            for (let i = 0; i < block.length; i += 2) {
                for (let j = 0; j < (block[i] == 0 ? 256 : block[i]); j++) {
                    flat.push(block[i + 1]);
                }
            }
        }

        // Unravel the attributes
        let attrs = flat.splice(30 * 32);
        this.attributes = [];
        for (let y = 0; y < 30; y += 2) {
            let row: number[] = []
            for (let x = 0; x < 32; x += 2) {
                const attr_i = (y >> 2) * 8 + (x >> 2);
                const attr_b = ((y >> 1) % 2 ? 4 : 0) + ((x >> 1) % 2 ? 2 : 0);
                row.push((attrs[attr_i] >> attr_b) & 3)
            }

            this.attributes.push(row);
        }

        // Build a 2D array from the flat array
        this.level = [];
        while (flat.length)
            this.level.push(flat.splice(0, 32));

        // Add the collision bitmap
        this.collision = []
        for (let y = 0; y < 30; y++) {
            let row: boolean[] = []
            for (let x = 0; x < 32; x += 8) {
                const byte = numbers[(y * 32 + x) >> 3];
                for (let j = 0; j < 8; j++)
                    row.push((byte & (0x80 >> j)) > 0);
            }
            this.collision.push(row);
        }
        numbers.splice(0, 30 * 32 / 8);

        // Add the death bitmap
        this.death = []
        for (let y = 0; y < 30; y++) {
            let row: boolean[] = []
            for (let x = 0; x < 32; x += 8) {
                const byte = numbers[(y * 32 + x) >> 3];
                for (let j = 0; j < 8; j++)
                    row.push((byte & (0x80 >> j)) > 0);
            }
            this.death.push(row);
        }
        numbers.splice(0, 30 * 32 / 8);

        // Parse the entity array
        this.entities = [];
        const entityCount = numbers.splice(0, 1)[0];
        for (let i = 0; i < entityCount; i++) {
            const row = numbers.splice(0, 4);
            const entity = new Entity(row[0], { x: row[1], y: row[2] }, row[3]);
            this.entities.push(entity);
        }
    }
}
