import App from '../components/app.vue';

export default class ChrLoader {
    app: App;

    constructor(app: App) {
        this.app = app;
    }

    // Parses a chr file and loads its sprites into this.sprites
    parseChr(content: ArrayBuffer) {
        this.app.sprites = [];

        for (let i = 0; i < 512; i++) {
            // Get an Uint8Array of the sprite data
            let slice = content.slice(i * 0x10, i * 0x10 + 0x10);
            let sprite = new Uint8Array(slice);

            let pattern: number[][] = [];
            for (let y = 0; y < 8; y++) {
                let row: number[] = [];
                for (let x = 0; x < 8; x++) {
                    // Calculate the pixel value
                    const high = (sprite[y + 8] >> (7 - x)) & 1;
                    const low  = (sprite[y]     >> (7 - x)) & 1;
                    row.push(high * 2 + low);
                }
                pattern.push(row);
            }

            this.app.sprites.push(pattern);
        }

        localStorage.setItem('sprites', JSON.stringify(this.app.sprites));
    }

    // Handles someone uploading a .chr file
    onChrUpload(e: UIEvent) {
        let target = e.target as HTMLInputElement;
        if (!target.files || target.files.length == 0)
            return;

        let reader = new FileReader();
        reader.onload = (ev: Event) => {
            var content = (ev.target as FileReader).result;
            this.parseChr(content);
        };

        reader.readAsArrayBuffer(target.files[0]);
    }
}
